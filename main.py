# AME - Agnostic Matronymic Engine
#
# Generate randomized names using simple english grammatical rules.
#
# Author: Peter Backlund
# Email: backlunp <at> gmail.com
# Date: 24-11-2015

#*********************************************************#
# Imports
#*********************************************************#
import string
import random

#*********************************************************#
# Global Variables
#*********************************************************#

var_len_low = 5     # Lower limit to name
var_len_high = 8    # Upper limit to name
var_num = 100    # No. of names to generate

const_alphabet = list(string.ascii_lowercase)   # List of alphabet
const_vowels = ['a', 'e', 'i', 'o', 'u']        # List of vowels
const_consonants = list(set(const_alphabet) ^ set(const_vowels))    # List of consonants

const_endings = ['tex',
                 'tec',
                 'co ',
                 'ia ',
                 'eon']


#*********************************************************#
# Main
#*********************************************************#

def main():

    list_names = []

    for iter in range(var_num):
        list_names.append(create_name(random.randrange(var_len_low, var_len_high))) #create a name of length var_len

    for name in list_names:
        print name + ' - ',
        for ending in const_endings:
            print "\t" + name + ending,

        print ' '


#*********************************************************#
# Routines
#*********************************************************#

def create_name(local_len):

    list_letters = list(random.choice(const_alphabet))

    while len(list_letters) < local_len:

        var_new_letter = random.choice(const_alphabet)

        if consonant_run(var_new_letter, list_letters):
            list_letters.append(var_new_letter)

    return ''.join(list_letters)


def consonant_run(var_cur, cur_list):
    if var_cur in const_consonants:
        if cur_list[-1] in const_consonants:
            if len(cur_list) > 1 and cur_list[-2] in const_consonants:
                return False # absolutely no three consonants in a row, that would be cray

    return True # Everything went ok



if __name__ == '__main__':
        main()